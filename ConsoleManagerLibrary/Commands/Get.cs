﻿using System;
using ConsoleManagerLibrary.Services;
using ConsoleMangerApi;

namespace ConsoleManagerLibrary.Commands
{
    public class Get : ICommand
    {
        public Get(string endPoint)
        {
            EndPoint = endPoint;
        }

        public Get(string endPoint, string id)
        {
            EndPoint = endPoint;
            Id = id;
        }

        private string EndPoint { get; }
        private string Id { get; }

        public void Run()
        {
            if (Id == null)
            {
                WebService.Get(EndPoint);
            }
            else
            {
                var id = Convert.ToInt32(Id);
                WebService.Get(EndPoint, id);
            }
        }
    }
}
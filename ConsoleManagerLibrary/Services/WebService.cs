﻿using System;
using System.Net;

namespace ConsoleManagerLibrary.Services
{
    public static class WebService
    {
        private const string NotFound = "HTTP: 404";

        public static void Get(string endpoint)
        {
            using var client = new WebClient();
            try
            {
                client.Headers.Add("Content-Type:application/json");
                client.Headers.Add("Accept:application/json");
                var result = client.DownloadString($"https://localhost:44338/api/{endpoint}");
                Console.WriteLine(Environment.NewLine + result);
            }
            catch (Exception)
            {
                Console.WriteLine(NotFound);
            }
        }

        public static void Get(string endpoint, long id)
        {
            using var client = new WebClient();
            try
            {
                client.Headers.Add("Content-Type:application/json");
                client.Headers.Add("Accept:application/json");
                var result = client.DownloadString($"https://localhost:44338/api/{endpoint}/{id}");
                Console.WriteLine(Environment.NewLine + result);
            }
            catch (Exception)
            {
                Console.WriteLine(NotFound);
            }
        }
    }
}
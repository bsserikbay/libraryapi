﻿using System;
using ConsoleManager;

internal class Program
{
    public static void Main(string[] args)
    {
        var parser = new CommandLineParser("ConsoleManagerLibrary");

        try
        {
            var command = parser.Parse(args);
            command.Run();
        }
        catch (Exception)
        {
            Console.WriteLine(
                "Format: \nRequest all items:     get [endpoint]\nRequest an item by id: get [endpoint] [id]");
        }
    }
}
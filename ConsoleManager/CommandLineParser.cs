﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ConsoleMangerApi;

namespace ConsoleManager
{
    public class CommandLineParser
    {
        private readonly string libName;

        public CommandLineParser(string libName)
        {
            this.libName = libName;
        }

        private bool TryParse(string[] args, out ICommand result)
        {
            result = null;

            var commandList = GetAssembly(libName)
                .GetTypes()
                .Where(p => typeof(ICommand).IsAssignableFrom(p));

            var command = commandList
                .FirstOrDefault(x => x.Name.ToLower() == args[0].ToLower());

            if (command == null)
            {
                Console.WriteLine("Check format!");
                Console.WriteLine("=====================");
                Console.WriteLine("Available commands:");

                foreach (var availableCommand in commandList)
                {
                    Console.WriteLine(availableCommand.Name);
                }

                Console.WriteLine("=====================");
            }
            
            else
                result = (ICommand) Activator.CreateInstance(command, args.Skip(1).ToArray());

            return true;
        }

        public ICommand Parse(string[] args)
        {
            if (TryParse(args, out var result)) return result;


            throw new FormatException();
        }


        private static Assembly GetAssembly(string name)
        {
            return GetAssemblies()
                .SingleOrDefault(x => x.GetName().Name == name);
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            var assemblies = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll")
                .Select(x => Assembly.Load(AssemblyName.GetAssemblyName(x)));
            return assemblies.ToArray();
        }
    }
}
﻿namespace ConsoleMangerApi
{
    public interface ICommand
    {
        void Run();
        
    }
}
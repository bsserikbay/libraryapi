﻿using System.Collections.Generic;
using System.Linq;
using LibraryApi.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryApi.Repositories
{
    public class AuthorRepository : BaseRepository<Author>
    {
        public AuthorRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
        }

        public List<Author> GetAuthors()
        {
            return _dbSet
                .Include(x => x.Books)
                .ToList();
        }

        public Author GetAuthor(long id)
        {
            return _dbSet
                .Include(x => x.Books)
                .SingleOrDefault(x => x.Id == id);
        }
    }
}
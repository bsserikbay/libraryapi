﻿using System.Linq;
using LibraryApi.Models;

namespace LibraryApi.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
        }

        public User GetAuthenticatedUser(AuthenticateRequest model)
        {
            return _dbSet.SingleOrDefault(x => x.Username == model.Username && x.Password == model.Password);
            
        }
    }
}
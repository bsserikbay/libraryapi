﻿using System.Collections.Generic;
using System.Linq;
using LibraryApi.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryApi.Repositories
{
    public class CategoryRepository : BaseRepository<Category>
    {
        public CategoryRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
        }

        public List<Category> GetAllCategories()
        {
            return _dbSet
                .Include(x => x.Books)
                .ToList();
        }

        public Category GetCategoryById(long id)
        {
            return _dbSet
                .Include(x => x.Books)
                .SingleOrDefault(x => x.Id == id);
        }
    }
}
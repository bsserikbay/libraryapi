﻿using System.Collections.Generic;
using System.Linq;
using LibraryApi.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryApi.Repositories
{
    public abstract class BaseRepository<DbModel> where DbModel : BaseModel
    {
        protected DbSet<DbModel> _dbSet;
        private readonly LibraryDbContext _libraryDbContext;

        protected BaseRepository(LibraryDbContext libraryDbContext)
        {
            _libraryDbContext = libraryDbContext;
            _dbSet = libraryDbContext.Set<DbModel>();
        }
        
        public List<DbModel> GetAll()
        {
            return _dbSet.ToList();
        }

        public DbModel GetById(long id)
        {
            return _dbSet.SingleOrDefault(x => x.Id == id);
        }

        public void Update(DbModel model)
        {
            _libraryDbContext.Entry(model).State = EntityState.Modified;
            _libraryDbContext.SaveChanges();
        }

        public void Save(DbModel model)
        {
            _dbSet.Add(model);
            _libraryDbContext.SaveChanges();
        }

        public void Remove(DbModel model)
        {
            _dbSet.Remove(model);
            _libraryDbContext.SaveChanges();
        }

        public void RemoveAll()
        {
            _dbSet.RemoveRange(_dbSet);
            _libraryDbContext.SaveChanges();
        }

        public void SaveRange(params DbModel[] values)
        {
            _dbSet.AddRange(values);
            _libraryDbContext.SaveChanges();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using LibraryApi.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryApi.Repositories
{
    public class BookRepository : BaseRepository<Book>
    {
        public BookRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
        }

        public List<Book> GetBooks()
        {
            return _dbSet
                .Include(x => x.Author)
                .ToList();
        }

        public Book GetBook(long id)
        {
            return _dbSet
                .Include(x => x.Author)
                .Include(x => x.Categories)
                .SingleOrDefault(x => x.Id == id);
        }

        public List<Book> GetSearchBook(string query)
        {
            return _dbSet
                .Include(x => x.Author)
                .Include(x => x.Categories)
                .Where(x =>
                    x.Title.Contains(query) ||
                    x.Author.Name.Contains(query)
                )
                .ToList();
        }
    }
}
﻿using System.Collections.Generic;

namespace LibraryApi.Models
{
    public class Book : BaseModel
    {
        public string Title { get; set; }
        public Author Author { get; set; }
        public List<Category> Categories { get; set; } = new();
    }
}
﻿using System.Text.Json.Serialization;

namespace LibraryApi.Models
{
    public class User : BaseModel
    {
       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }

      //  [JsonIgnore]
        public string Password { get; set; }
    }
}
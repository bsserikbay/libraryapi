﻿using System;
using LibraryApi.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LibraryApi.Models
{
    public static class SeedExtension
    {
        public static IHost Seed(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                CreateDefaultData(scope.ServiceProvider);
            }

            return host;
        }


        private static void CreateDefaultData(IServiceProvider serviceProvider)
        {
            var bookRepository = serviceProvider.GetService<BookRepository>();
            var categoryRepository = serviceProvider.GetService<CategoryRepository>();
            var authorRepository = serviceProvider.GetService<AuthorRepository>();
            var userRepository = serviceProvider.GetService<UserRepository>();

            bookRepository.RemoveAll();
            categoryRepository.RemoveAll();
            authorRepository.RemoveAll();
            userRepository.RemoveAll();
            
            
            var admin = new User()
            {
                FirstName = "Test", 
                LastName = "User", 
                Username = "test", 
                Password = "test"
            };
            userRepository.Save(admin);

            var madeline = new Author
            {
                Name = "Madeline Miller"
            };

            var cathryn = new Author
            {
                Name = "Cathryn Stockett"
            };

            authorRepository.SaveRange(madeline, cathryn);

            var circle = new Book
            {
                Title = "Circle",
                Author = madeline
            };

            var help = new Book
            {
                Title = "The Help",
                Author = cathryn
            };

            var sun = new Book
            {
                Title = "She Who Became the Sun",
                Author = cathryn
            };

            var adventure = new Category
            {
                CategoryName = "Action and Adventure"
            };

            var detective = new Category
            {
                CategoryName = "Detective and Mystery"
            };

            var comedy = new Category
            {
                CategoryName = "Comedy"
            };

            var crime = new Category
            {
                CategoryName = "True Crime"
            };

            categoryRepository.SaveRange(detective, adventure, comedy, crime);

            circle.Categories.Add(adventure);
            circle.Categories.Add(comedy);

            help.Categories.Add(adventure);
            help.Categories.Add(detective);
            help.Categories.Add(crime);

            sun.Categories.Add(adventure);

            bookRepository.SaveRange(circle, help, sun);
        }
    }
}
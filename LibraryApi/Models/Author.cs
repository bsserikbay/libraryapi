﻿using System.Collections.Generic;

namespace LibraryApi.Models
{
    public class Author : BaseModel
    {
        public string Name { get; set; }

        public List<Book> Books { get; set; }
    }
}
﻿namespace LibraryApi.Models
{
    public class SearchTerm : BaseModel
    {
        public string Query { get; set; }
    }
}
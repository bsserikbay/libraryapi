﻿namespace LibraryApi.Models
{
    public abstract class BaseModel
    {
        public long Id { get; set; }
    }
}
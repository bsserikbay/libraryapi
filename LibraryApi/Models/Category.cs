﻿using System.Collections.Generic;

namespace LibraryApi.Models
{
    public class Category : BaseModel
    {
        public string CategoryName { get; set; }

        public List<Book> Books { get; set; } = new();
    }
}
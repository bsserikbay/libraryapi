﻿using LibraryApi.Helpers;
using LibraryApi.Models;
using LibraryApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly AuthorRepository _authorRepository;

        public AuthorsController(AuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        [HttpGet]
        public IActionResult GetAuthors()
        {
            var authors = _authorRepository.GetAuthors();

            return Ok(authors);
        }

        [HttpGet("{id:long}")]
        public IActionResult GetAuthor(long id)
        {
            var author = _authorRepository.GetAuthor(id);

            if (author == null) return NotFound();

            return Ok(author);
        }
        [Authorize]
        [HttpPut("{id:long}")]
        public IActionResult PutAuthor(long id, Author author)
        {
            if (id != author.Id) return BadRequest();

            _authorRepository.Update(author);

            return NoContent();
        }
        [Authorize]
        [HttpPost]
        public IActionResult PostAuthor(Author author)
        {
            _authorRepository.Save(author);

            return CreatedAtAction("GetAuthor", new {id = author.Id}, author);
        }
        [Authorize]
        [HttpDelete("{id:long}")]
        public IActionResult DeleteAuthor(long id)
        {
            var author = _authorRepository.GetById(id);

            if (author == null) return NotFound();

            _authorRepository.Remove(author);

            return NoContent();
        }
    }
}
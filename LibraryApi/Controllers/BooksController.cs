﻿using LibraryApi.Helpers;
using LibraryApi.Models;
using LibraryApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BookRepository _bookRepository;

        public BooksController(BookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        [HttpGet]
        public IActionResult GetBooks()
        {
            var books = _bookRepository.GetBooks();

            return Ok(books);
        }

        [HttpGet("{id:long}")]
        public IActionResult GetBook(long id)
        {
            var book = _bookRepository.GetBook(id);

            if (book == null) return NotFound();

            return Ok(book);
        }
        [Authorize]
        [HttpPut("{id:long}")]
        public IActionResult PutBook(long id, Book book)
        {
            if (id != book.Id) return BadRequest();

            _bookRepository.Update(book);

            return NoContent();
        }
        [Authorize]
        [HttpPost]
        public IActionResult PostBook(Book book)
        {
            _bookRepository.Save(book);

            return CreatedAtAction("GetBook", new {id = book.Id}, book);
        }
        [Authorize]
        [HttpDelete("{id:long}")]
        public IActionResult DeleteBook(long id)
        {
            var book = _bookRepository.GetById(id);
            if (book == null) return NotFound();

            _bookRepository.Remove(book);
            return NoContent();
        }
    }
}
﻿using LibraryApi.Models;
using LibraryApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly BookRepository _bookRepository;

        public SearchController(BookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        [HttpPost]
        public IActionResult Search(SearchTerm term)
        {
            var query = term.Query;

            var result = _bookRepository.GetSearchBook(query);

            return Ok(result);
        }
    }
}
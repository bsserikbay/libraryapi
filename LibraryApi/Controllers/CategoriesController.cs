﻿using LibraryApi.Helpers;
using LibraryApi.Models;
using LibraryApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly CategoryRepository _categoryRepository;

        public CategoriesController(CategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public IActionResult GetCategories()
        {
            var categories = _categoryRepository.GetAllCategories();

            return Ok(categories);
        }

        [HttpGet("{id:long}")]
        public IActionResult GetCategory(long id)
        {
            var category = _categoryRepository.GetCategoryById(id);

            if (category == null) return NotFound();

            return Ok(category);
        }


        [HttpPut("{id:long}")]
        public IActionResult PutCategory(long id, Category category)
        {
            if (id != category.Id) return BadRequest();

            _categoryRepository.Update(category);

            return NoContent();
        }
        [Authorize]
        [HttpPost]
        public IActionResult PostCategory(Category category)
        {
            _categoryRepository.Save(category);

            return CreatedAtAction("GetCategory", new {id = category.Id}, category);
        }
        [Authorize]
        [HttpDelete("{id:long}")]
        public IActionResult DeleteCategory(long id)
        {
            var category = _categoryRepository.GetById(id);
            if (category == null) return NotFound();

            _categoryRepository.Remove(category);

            return NoContent();
        }
    }
}
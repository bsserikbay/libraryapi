﻿using LibraryApi.Helpers;
using LibraryApi.Models;
using LibraryApi.Repositories;
using LibraryApi.Services;
using Microsoft.AspNetCore.Mvc;
using BC = BCrypt.Net.BCrypt;

namespace LibraryApi.Controllers
{
        [Route("api/[controller]")]
        [ApiController]
        public class UsersController : ControllerBase
        {
            private IUserService _userService;
            private UserRepository _userRepository;

            public UsersController(IUserService userService, UserRepository userRepository)
            {
                _userService = userService;
                _userRepository = userRepository;
            }
            
            [HttpPost("/register")]
            public void Register(User user)
            {
                var hasPassword = BC.HashPassword(user.Password);

                var newUser = new User
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Username = user.Username,
                    Password = hasPassword
                };

                _userRepository.Save(newUser);
            }
            

            [HttpPost("authenticate")]
            public IActionResult Login(AuthenticateRequest model)
            {
                var response = _userService.Authenticate(model);

                if (response == null)
                    return BadRequest(new { message = "Username or password is incorrect" });

                return Ok(response);
            }

            [Authorize]
            [HttpGet]
            public IActionResult GetAll()
            {
                var users = _userRepository.GetAll();
                return Ok(users);
            }
        }
}